package com.sbim.loan.util;

public class AppUtil {

	public static LoanStatus getLoanStatus(String status) {

		if (status.equalsIgnoreCase("NEW")) {
			return LoanStatus.NEW;

		} else if (status.equalsIgnoreCase("APPROVED")) {
			return LoanStatus.APPROVED;
		} else if (status.equalsIgnoreCase("IN-PROCESS")) {
			return LoanStatus.IN_PROCESS;
		} else if (status.equalsIgnoreCase("DECLINED")) {
			return LoanStatus.DECLINED;
		} else {

			return LoanStatus.CLOSED;
		}

	}

}
