package com.sbim.loan.util;

public enum LoanStatus {

	NEW,
	APPROVED,
	IN_PROCESS,
	DECLINED,
	CLOSED
	
}
