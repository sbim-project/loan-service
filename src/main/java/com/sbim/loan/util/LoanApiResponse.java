package com.sbim.loan.util;

import com.sbim.loan.model.LoanModel;

import lombok.Data;

@Data
public class LoanApiResponse {
	
	LoanModel loan_details;
	
	String status;

}
