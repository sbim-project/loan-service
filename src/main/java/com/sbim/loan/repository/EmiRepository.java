package com.sbim.loan.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sbim.loan.model.EmiModel;

public interface EmiRepository extends JpaRepository<EmiModel, String>{

}
