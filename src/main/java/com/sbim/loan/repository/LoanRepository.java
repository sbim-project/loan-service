package com.sbim.loan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sbim.loan.model.LoanModel;

@Repository
public interface LoanRepository extends JpaRepository<LoanModel, String>{
	
	
	List<LoanModel> findByCustomerid(String customerid);
	
	LoanModel findByCustomeridAndLoanId(String customerid, String loanid);

}
