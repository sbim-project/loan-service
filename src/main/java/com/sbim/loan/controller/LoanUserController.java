package com.sbim.loan.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sbim.loan.model.EmiModel;
import com.sbim.loan.model.LoanModel;
import com.sbim.loan.service.LoanService;
import com.sbim.loan.util.LoanApiResponse;

@RestController
@RequestMapping("/loan/user/api/")
public class LoanUserController {

	@Autowired
	LoanService loanService;

	@PostMapping("loans")
	public ResponseEntity<LoanApiResponse> applyLoan(@RequestBody LoanModel loanModel) {

		LoanApiResponse response = new LoanApiResponse();

		LoanModel model = null;

		int acc_balance = 100000;
		int credit_score = 700;

		if (acc_balance >= 200000 && credit_score >= 800) {
			model = loanService.saveLoan(loanModel);
			response.setStatus("Loan Applied Successfully..!!!");
			response.setLoan_details(model);
		} else {
			response.setStatus("Loan Application failed, You are not allowed to take loan.");
			response.setLoan_details(model);
		}

		if (model != null) {
			return new ResponseEntity<LoanApiResponse>(response, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("customer/{customerid}/loans")
	public ResponseEntity<List<LoanModel>> getAllLoansByCustomerId(@PathVariable("customerid") String customerid) {

		List<LoanModel> list = loanService.getAllLoansByCustomerId(customerid);

		if (list != null) {
			return new ResponseEntity<List<LoanModel>>(list, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("customer/{customerid}/loans/{loanid}")
	public ResponseEntity<LoanModel> loanDetailsById(@PathVariable("customerid") String customerid,
			@PathVariable("loanid") String loanid) {

		LoanModel details = loanService.loanDetailsById(customerid, loanid);

		if (details != null) {
			return new ResponseEntity<LoanModel>(details, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}

	}
	
	@PutMapping("customer/loans")
	public ResponseEntity<EmiModel> payEMI(@RequestBody EmiModel request) {

		EmiModel emiResponse = loanService.payEMI(request);

		if (emiResponse != null) {
			return new ResponseEntity<EmiModel>(emiResponse, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	

}
