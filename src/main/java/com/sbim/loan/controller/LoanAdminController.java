package com.sbim.loan.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sbim.loan.model.LoanModel;
import com.sbim.loan.service.LoanService;

@RestController
@RequestMapping("/loan/admin/api/")
public class LoanAdminController {

	@Autowired
	LoanService loanService;
	
	@PutMapping("updatestatus/{loanid}/{status}")
	public ResponseEntity<LoanModel> updateLoanStatus(@PathVariable("loanid") String loanid , @PathVariable("status") String status) {

		LoanModel model = loanService.updateLoanStatus(loanid, status);

		if (model != null) {
			return new ResponseEntity<LoanModel>(model, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@GetMapping("allLoans")
	public ResponseEntity<List<LoanModel>> getAllLoans(){
		
		List<LoanModel> list = loanService.getAllLoans();
		
		if (list != null) {
			return new ResponseEntity<List<LoanModel>>(list, HttpStatus.OK);
			
		} else {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		
	}

	

}
