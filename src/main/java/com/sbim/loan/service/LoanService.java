package com.sbim.loan.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.sbim.loan.model.EmiModel;
import com.sbim.loan.model.LoanModel;

@Service
public interface LoanService {

	public LoanModel saveLoan(LoanModel loanModel);

	public List<LoanModel> getAllLoansByCustomerId(String customerid);

	public LoanModel loanDetailsById(String customerid, String loanid);

	public EmiModel payEMI(EmiModel request);

	public LoanModel updateLoanStatus(String loanid , String status);

	public List<LoanModel> getAllLoans();

}
