package com.sbim.loan.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbim.loan.model.EmiModel;
import com.sbim.loan.model.LoanModel;
import com.sbim.loan.repository.EmiRepository;
import com.sbim.loan.repository.LoanRepository;
import com.sbim.loan.util.AppUtil;
import com.sbim.loan.util.LoanStatus;

@Service
public class LoanServiceImpl implements LoanService {

	@Autowired
	LoanRepository loanRepository;

	@Autowired
	EmiRepository emiRepository;

	@Override
	public LoanModel saveLoan(LoanModel loanModel) {
		loanModel.setBalance_amount(loanModel.getLoan_amount());
		loanModel.setBalance_tenure(loanModel.getTenure());
		loanModel.setStatus(LoanStatus.NEW);
		return loanRepository.save(loanModel);
	}

	@Override
	public List<LoanModel> getAllLoansByCustomerId(String customerid) {

		return loanRepository.findByCustomerid(customerid);
	}

	@Override
	public LoanModel loanDetailsById(String customerid, String loanid) {

		return loanRepository.findByCustomeridAndLoanId(customerid, loanid);
	}

	@Override
	public EmiModel payEMI(EmiModel request) {

		LoanModel model = loanRepository.findByCustomeridAndLoanId(request.getCustomerid(), request.getLoanId());

		int balanceamt = model.getBalance_amount() - request.getAmount();
		model.setBalance_amount(balanceamt);

		int balancetenure = model.getBalance_tenure() - 1;
		model.setBalance_tenure(balancetenure);
		

		return emiRepository.save(request);
	}

	@Override
	public LoanModel updateLoanStatus(String loanid, String status) {

		LoanModel model = loanRepository.getById(loanid);

		model.setStatus(AppUtil.getLoanStatus(status));

		return loanRepository.save(model);
	}

	@Override
	public List<LoanModel> getAllLoans() {
		
		return loanRepository.findAll();
	}

}
